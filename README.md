# youtube-importer

The youtube importer is an script written for any-and-all internet connoisseurs that would like to have an easy way to import their youtube playlists and subscriptions using the data provided by google takeout to facilitate the migration between youtube acounts.

## Prerequesites

The requests made by this script need to be provided with an OAuth 2.0 token. 
This can be aquired by logging into the google account **that will import the data** and accessing the [Google Developers Console](https://console.developers.google.com/).
In the Developers Console:
1. Create a project 
1. Obtain the necessary [authorization credentials](https://developers.google.com/youtube/registering_an_application) located at `Credentials > +create credentials`
    - Make sure to select the option `OAuth client ID`.
    - If prompted to set up an `OAuth consent screen`, do that beforehand, making sure to select the `YouTube Data API` scope regarding acount management during setup.
    - Specify `Desktop app` as Application type
1. After setting up the `OAuth client ID` make sure to download the `client secret JSON file`.
    - This allows the script to authorize all requests. 
1. Add your google accounts as OAuth test users in the `OAuth consent screen`.
    - This allows your accounts to make use of the script.

After the creation of your project make sure the YouTube Data API is one of the services that your application is registered to use.
In the Developers Console:
 1. Open the [Enabled APIs and services](https://console.cloud.google.com/apis/dashboard) dahsboard.
 1. Select `+ENABLE APIS AND SERVICES`.
 1. Look for the YouTube Data API v3 and make sure its **ENABLED**.

 **DISCLAMER:** most of this information is retrieved from the Youtube Data API documentation. Any missing information can be found in the [documentation](https://developers.google.com/youtube/v3/).

## Usage

### Dependencies

The dependencies are installed using pip:
```python
python -m pip install -r ./requirements.txt
```

### Authorization

When executing the script the user will be prompted to authorize the application by visiting the URL presented in the command-line interfacve and entering the authorization code shown in the browser. This will happen everytime the script is executed due to the script ending it's session after execution.


**Important Note!**

The Youtube Data API has a query limit of 10,000 points per day (see quota costs [here](https://developers.google.com/youtube/v3/determine_quota_cost)) thus, depending on the amount of data to import, the import might have to be spread over a few days. Alternatively, if applicable, the user can contact google to [apply for a higher quota](https://support.google.com/youtube/contact/yt_api_form).

### Importing

Subscriptions can be imported as follows:
```
python import.py --subscriptions --path /path/to/file --auth /path/to/client-secret
```

A playlist can be imported as follows:
```
python import.py --playlist --path /path/to/file --auth /path/to/client-secret
```

It is also possible to import multiple playlists by providing the path to the directory containing the playlists:
```
python import.py --playlists --path /path/to/directory --auth /path/to/client-secret
```

Aditionally, the user can set the level of dept of the logfile as shown in the example below:
```
python import.py --playlist --path /path/to/file --auth /path/to/client-secret --log-level DEBUG
```
*The log level is set to debug by default.*

**Important Note!**

Importing an already existing playlist will simply update the existing playlist. However it won't check for duplicate videos and merely append the videos to the playlist. Therefore, it is recommended to remove the already imported videos from the playlist's CSV file when updating an existing playlist. This is especially recommended when importing larger playlists over the span of a few days.