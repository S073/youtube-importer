#!/usr/bin/env python3

import argparse
import csv
import json
import logging
import os
import sys
from datetime import datetime

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors as api_errors

# Disable OAuthlib's HTTPS verification when running locally.
# *DO NOT* leave this option enabled in production.
os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
API_SERVICE_NAME = "youtube"
API_VERSION = "v3"
SCOPES = [
    "https://www.googleapis.com/auth/youtube",
    ]


def create_API_client(path):
    """
        Sets up an API client for executing api methods.
    """
    
    try:
        flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(path, SCOPES)
        credentials = flow.run_console()
        return googleapiclient.discovery.build(API_SERVICE_NAME, API_VERSION, credentials=credentials)
    except ValueError:
        logging.info("Invalid format provided. Please insert the propper client secret JSON file.")
        print("Invalid format provided. Please insert the propper client secret JSON file.")


class Client():
    """
        The application client.
    """

    def __init__(self, client_secret, path, loglevel=logging.DEBUG):
        logging.basicConfig(filename=f"log_{datetime.now()}.txt", level=loglevel, format='')
        self.youtube = create_API_client(client_secret)
        self.path = path
        self.request = ""
        self.response = ""
        self.existing_playlists = []


    def __execute_request(self):
        try:
            self.response = self.request.execute()
        except api_errors.HttpError as http_err:
            logging.info(http_err)
            print(http_err.reason)
        except Exception as e:
            logging.info(e)
            print(e)


    def __playlist_exists(self, playlist_name):
        for playlist in self.existing_playlists:
            if playlist["title"] == playlist_name:
                return playlist
        
        return False


    def get_existing_playlists(self):
        self.request = self.youtube.playlists().list(
            part="snippet,contentDetails",
            maxResults=50,
            mine=True
        )
        self.__execute_request()
        
        for playlist in self.response["items"]:
            self.existing_playlists += [{"channelId": playlist["snippet"]["channelId"], "title": playlist["snippet"]["title"], "id": playlist["id"]}]
 

    def import_subscription(self):
        try:
            with open(self.path, 'r') as csv_file:
                reader = csv.reader(csv_file)
                fields = next(reader)
                
                for values in reader:
                    row = dict(zip(fields, values))
                    
                    self.request = self.youtube.subscriptions().insert(
                        part="snippet",
                        body={
                            "snippet": {
                                "resourceId": {
                                "kind": "youtube#channel",
                                "channelId": row["Channel ID"] # channel ID
                                }
                            }
                        }
                    )
                    self.__execute_request()
                    
                    if self.response:
                        logging.info(f"Subscribed to {self.response['snippet']['title']}")
                        print("Subscribed to", self.response["snippet"]["title"])
                        
                logging.info("Subscriptions imported successfully!")
                print("Subscriptions imported successfully!")
        except IsADirectoryError:
            logging.warning("The provided path is a directory. It should point to a file instead.")
            print("The provided path is a directory. It should point to a file instead.")


    def import_playlists(self):
        try:
            for root, dirs, files in os.walk(self.path):
                for file in files:
                    self.import_playlist(path_overwrite=os.path.join(root, file))
        except NotADirectoryError:
            logging.warning("The provided path does not point to a directory.")
            print("The provided path does not point to a directory.")


    def import_playlist(self, path_overwrite=None):
        try:
            if path_overwrite:
                self.path = path_overwrite

            with open(self.path, 'r') as csv_file:
                reader = csv.reader(csv_file)
                fields = next(reader)
                values = next(reader)
                csv_data = dict(zip(fields, values))
                next(reader) # skip whitespace between playlist and video data
                
                playlist = self.__playlist_exists(csv_data["Title"])
                if not playlist:
                    print("Playlist " + csv_data["Title"] +" does not exist. Creating Playlist...")
                    self.request = self.youtube.playlists().insert(
                        part="snippet,status",
                        body={
                            "snippet": {
                                "title": csv_data["Title"],
                                "description": csv_data["Description"],
                                "defaultLanguage": "en"
                            },
                            "status": {
                                "privacyStatus": csv_data["Visibility"]
                            }
                        }
                    )
                    self.__execute_request()
                    logging.info(f'Playlist "{csv_data["Title"]}" created successfully!')
                    print("Playlist created successfully!")
                    self.update_playlist(self.response["id"], reader)
                else:
                    playlist = ""
                    for playlist_data in self.existing_playlists:
                        if playlist_data["title"] == csv_data["Title"]:
                            playlist = playlist_data

                    self.update_playlist(playlist["id"], reader)
        except IsADirectoryError:
            logging.warning("The provided path is a directory. It should point to a file instead.")
            print("The provided path is a directory. It should point to a file instead.")


    def update_playlist(self, playlist_id, reader):
        logging.info("Appending videos to playlist...")
        print("Appending videos to playlist...")
        fields = next(reader)
        for values in reader:
            if values:
                row = dict(zip(fields, values))
                logging.info(f"Adding video ID: {row['Video ID']}")
                print("Adding video ID:", row["Video ID"])
                self.request = self.youtube.playlistItems().insert(
                    part="snippet",
                    body={
                        "snippet": {
                            "playlistId": playlist_id,
                            "position": 0,
                            "resourceId": {
                            "kind": "youtube#video",
                            "videoId": row["Video ID"]
                            }
                        }
                    }
                )
                self.__execute_request()

        logging.info("Videos imported successfully!")        
        print("Videos imported successfully!")


def main():
    parser = argparse.ArgumentParser(description="Import youtube playlists and subscriptions.")
    resource = parser.add_mutually_exclusive_group(required=True)
    resource.add_argument("--subscriptions", action="store_true", help="Import subscriptions.")
    resource.add_argument("--playlist", action="store_true", help="Import one playlist.")
    resource.add_argument("--playlists", action="store_true", help="Import multiple playlists.")
    parser.add_argument("-p", "--path", metavar="PATH", type=str, required=True, help="The path to the csv file or directory. Should be a directory path if --playlist flag is raised.")
    parser.add_argument("--auth", metavar="PATH", type=str, required=True, help="The client authentication secret file.")
    parser.add_argument("-ll", "--log-level", choices=['CRITICAL', 'FATAL', 'ERROR', 'WARN', 'WARNING', 'INFO', 'DEBUG', 'NOTSET'], help="Set the log-level of the logfile.")
    args = parser.parse_args()
    args = vars(args)

    # display help message when no args are passed.
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1) 
    
    client = Client(args["auth"], args["path"])
    
    if args["subscriptions"]:
        client.import_subscription()

    client.get_existing_playlists()
    if args["playlist"]:
        client.import_playlist()
    elif args["playlists"]:
        client.import_playlists()


if __name__ == "__main__":main()